//
//  Disciplina.swift
//  Mobile
//
//  Created by Breno Pinheiro Aquino on 08/09/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import Foundation

class Disciplina {
    
    var nome: String = ""
    
    var numTurmas: Int = 1
    
    init(nome: String, numTurmas: Int) {
        
        self.nome = nome
        
        self.numTurmas = numTurmas
    }
}
