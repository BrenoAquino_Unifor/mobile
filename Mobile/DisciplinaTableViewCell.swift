//
//  DisciplinaTableViewCell.swift
//  Mobile
//
//  Created by Breno Pinheiro Aquino on 06/09/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class DisciplinaTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var viewEntreCells: UIView!
    
    @IBOutlet weak var disciplinaNomeView: UIView!
    
    @IBOutlet weak var nomeDisciplina: UILabel!
    
    @IBOutlet weak var numCredito: UILabel!
    
    @IBOutlet weak var backgroundCell: UIView!
    
    @IBOutlet weak var tableViewTurmas: UITableView!
    
    var numTurmas: Int = 2
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.selectionStyle = .none
        
        self.viewEntreCells.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        
        self.backgroundCell.layer.cornerRadius = 5
        
        self.backgroundCell.clipsToBounds = true
        
        self.tableViewTurmas.delegate = self
        
        self.tableViewTurmas.dataSource = self
        
        self.tableViewTurmas.register(UINib(nibName: "TurmaTableViewCell", bundle: nil), forCellReuseIdentifier: "cellTurma")
        
        self.tableViewTurmas.isScrollEnabled = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numTurmas
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableViewTurmas.dequeueReusableCell(withIdentifier: "cellTurma", for: indexPath) as! TurmaTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(81)
    }
}
