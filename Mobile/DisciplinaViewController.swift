//
//  DisciplinaViewController.swift
//  Mobile
//
//  Created by Breno Aquino on 01/09/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit
import SwifterSwift
import KMNavigationBarTransition
import AMScrollingNavbar

class DisciplinaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let colorsNumber = ["#fff16364",
                        "#fff58559",
                        "#fff9a43e",
                        "#ffe4c62e",
                        "#ff67bf74",
                        "#ff59a2be",
                        "#ff2093cd",
                        "#ffad62a7",
                        "#ff805781",
                        "#fff16364",
                        "#fff58559",
                        "#fff9a43e",
                        "#ffe4c62e",
                        "#ff67bf74",
                        "#ff59a2be",
                        "#ff2093cd",
                        "#ffad62a7",
                        "#ff805781"]
    
    let disciplinaNome = ["Sistemas Inteligentes",
                          "Teoria os Grafos",
                          "Principios de Controle",
                          "Calculo Numerico",
                          "Programacao Orientada a Objetos",
                          "Estrutura de Dados",
                          "Banco de Dados",
                          "Compiladores",
                          "Microprocessadores",
                          "Sistemas Lineares",
                          "Gestao Ambiental",
                          "Sistemas Logicos",
                          "Sistemas de Tempo Real",
                          "Economia",
                          "Aquisicao de Dados",
                          "Seguranca do Trabalho",
                          "Calculo I",
                          "Calculo II"]
    
    var disciplinas: [Disciplina] = []
    
    var colors: [UIColor] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var fazerDiferente = 0
        
        for nome in self.disciplinaNome {
            
            let toAdd: Disciplina!
            
            if fazerDiferente % 2 == 0 {
                
                toAdd = Disciplina(nome: nome, numTurmas: 2)
                
            } else {
                
                toAdd = Disciplina(nome: nome, numTurmas: 1)
            }
            
            fazerDiferente += 1
            
            self.disciplinas.append(toAdd)
        }
        
        for color in self.colorsNumber {
            
            self.colors.append(UIColor(hexString: color)!)
        }
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(color: #colorLiteral(red: 0.1401011944, green: 0.4582335949, blue: 0.7510772347, alpha: 1), size: CGSize(width: 1, height: 1)), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage(color: #colorLiteral(red: 0.1401011944, green: 0.4582335949, blue: 0.7510772347, alpha: 1), size: CGSize(width: 1, height: 1))
        
        self.tableView.register(UINib(nibName: "DisciplinaTableViewCell", bundle: nil), forCellReuseIdentifier: "cellDisciplinaDados")
    }
    
    // MARK: - Prepare for Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueDisciplinaDetalhes" {
            
            if let controller = segue.destination as? DisciplinaPageMenuViewController {
                
                if let indexPath = self.tableView.indexPathForSelectedRow {
                
                    controller.color = colors[indexPath.row]
                    
                    controller.nomeDisciplina = disciplinaNome[indexPath.row]
                }
            }
        }
    }
    
    // MARK: - TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.disciplinas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellDisciplinaDados", for: indexPath) as! DisciplinaTableViewCell
              
        cell.disciplinaNomeView.backgroundColor = colors[indexPath.row]
        
        cell.nomeDisciplina.text = "\(disciplinas[indexPath.row].nome)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(35 + self.disciplinas[indexPath.row].numTurmas * 82)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "segueDisciplinaDetalhes", sender: nil)
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}
