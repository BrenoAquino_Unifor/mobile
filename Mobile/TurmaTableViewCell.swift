//
//  TurmaTableViewCell.swift
//  Mobile
//
//  Created by Breno Pinheiro Aquino on 08/09/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class TurmaTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }
}
